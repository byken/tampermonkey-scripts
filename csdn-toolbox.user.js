// ==UserScript==
// @name         CSDN Toolbox
// @namespace    http://tampermonkey.net/
// @version      0.2.3
// @description  简单解决CSDN内容复制限制
// @author       byken
// @match        https://*.blog.csdn.net/*
// @icon         https://blog.csdn.net/favicon.ico
// @downloadURL  https://gitlab.com/byken/tampermonkey-scripts/raw/master/csdn-toolbox.user.js
// @updateURL    https://gitlab.com/byken/tampermonkey-scripts/raw/master/csdn-toolbox.user.js
// @run-at       document-start
// @grant        none
// ==/UserScript==

(function () {
  'use strict';
  const style = document.createElement('style')
  style.innerText = `
        pre,
        pre code {
            user-select: initial!important
        }
        #article_content{
            height: fit-content!important
        }
        .hide-article-box{
            visibility: hidden!important
        }
    `.split('\n').map(it => it.trim()).join('')
  document.head.appendChild(style)
  const realHTMLElementAddEventListener = HTMLElement.prototype.addEventListener
  HTMLElement.prototype.addEventListener = function (...args) {
    if (args[0] === 'copy' || args[0] === 'focus' || args[0] === 'mousemove') {
      return
    }
    realHTMLElementAddEventListener.apply(this, args)
  }
  let _csdn
  Object.defineProperty(window, 'csdn', {
    get: () => {
      return _csdn
    },
    set: (val) => {
      if (val?.report) {
        Object.keys(val.report).forEach(it => {
          if (typeof val.report[it] === 'function') {
            val.report[it] = () => { }
          }
        })
      }
      _csdn = val
    }
  })
  const realXMLHTPRequestOpen = XMLHttpRequest.prototype.open
  const realXMLHTPRequestSend = XMLHttpRequest.prototype.send
  XMLHttpRequest.prototype.open = function (...args) {
    if (/ev[^\.]*.csdn.net/.test(args[1])) {
      this._url = args[1]
      return
    }
    return realXMLHTPRequestOpen.apply(this, args)
  }
  XMLHttpRequest.prototype.send = function (...args) {
    if (/ev[^\.]*.csdn.net/.test(this._url)) {
      this.status = 200
      this.statusText = "OK"
      this.readyState = 4
      return
    }
    return realXMLHTPRequestSend.apply(this, args)
  }
  const realFetch = window.fetch

  window.fetch = function (...args) {
    if (/ev[^\.]*.csdn.net/.test(args[0])) {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(new Response(null, { status: 200, statusText: 'OK' }))
        })
      })
    }
    return realFetch.apply(window, args)
  }
})();