# Tampermonkey 油猴脚本汇总

1. [禁止 Huya P2P 上传](https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/huya-toolbox.user.js)（`huya-toolbox.user.js`）
> 禁止 虎牙直播进行 p2p 上传

2. [B站工具箱](https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/bilibili-toolbox.user.js)（`bilibili-toolbox.user.js`）
> 一系列优化项

3. [去除CSDN的内容拷贝限制](https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/csdn-toolbox.user.js)（`csdn-toolbox.user.js`）

4. [爱奇艺工具箱](https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/iqiyi-toolbox.user.js)(`iqiyi-toolbox.user.js`)
> 跳过广告、禁止P2P

1. [斗鱼工具箱](https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/douyu-toolbox.user.js)（`douyu-toolbox.user.js`）
> 简化播放页界面