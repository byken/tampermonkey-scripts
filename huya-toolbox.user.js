// ==UserScript==
// @name         Huya Toolbox.
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  简单、直接有效的禁用 huya P2P上传的方式
// @author       byken
// @match        https://www.huya.com/*
// @icon         https://www.huya.com/favicon.ico
// @grant        none
// @downloadURL  https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/huya-toolbox.user.js
// @updateURL    https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/huya-toolbox.user.js
// ==/UserScript==

(function () {
  'use strict';
  // 提供 P2P 支持的包为
  // https://a.msstatic.com/huya/h5player/room/<roomId>/p2plib.js
  // 导出全局对象 P2PLib，我们通过直接将 P2PLib 属性定义为 null，且不可写、不可配置，让 player.js 中的 P2P 功能失效
  Object.defineProperty(window, 'P2PLib', {
    value: null,
    configurable: false,
    enumerable: false,
    writable: false
  })
})();