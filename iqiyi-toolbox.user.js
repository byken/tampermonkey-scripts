// ==UserScript==
// @name         IQIYI toolbox
// @namespace    http://tampermonkey.net/
// @version      0.2.5
// @description  爱奇艺工具箱.
// @author       byken
// @match        https://www.iqiyi.com/v_*
// @icon         https://www.iqiyi.com/favicon.ico
// @downloadURL  https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/iqiyi-toolbox.user.js
// @updateURL    https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/iqiyi-toolbox.user.js
// @run-at       document-start
// @grant        none
// ==/UserScript==

(function () {
  'use strict';
  const $ = (...args) => args.length > 0 ? args[0] instanceof HTMLElement ? args[0].querySelectorAll(args.slice(1)) : window.document.querySelectorAll(...args) : null
  function findMatch(root, selectors, callback, once = false) {
    let matched = $(root, selectors)
    if (matched.length > 0) {
      matched.forEach(item => callback(item))
      if (once) {
        return
      }
    }
    /** @type {MutationCallback} */
    function mutationCallback(mutationsList, observer) {
      for (const mutationRecord of mutationsList) {
        for (const node of mutationRecord.addedNodes) {
          if (node.matches && node.matches(selectors)) {
            callback(node)
            if (once) {
              observer.disconnect()
              break
            }
            // 排除 TEXT_NODE
          } else if (node.querySelectorAll) {
            const matched = node.querySelectorAll(selectors)
            if (matched.length > 0) {
              matched.forEach(item => callback(item))
              if (once) {
                observer.disconnect()
                break
              }
            }
          }
        }
      }
    }
    const observer = new MutationObserver(mutationCallback)
    observer.observe(root, { childList: true, subtree: true })
  }

  function whenStyleChange(element, callback, once = false) {
    /** @type {MutationCallback} */
    function mutationCallback(mutationsList, observer) {
      mutationsList = mutationsList.filter(it => it.attributeName === 'style')
      for (const mutationRecord of mutationsList) {
        callback(mutationRecord.target)
        if (once) {
          observer.disconnect()
          break
        }
      }
    }
    const observer = new MutationObserver(mutationCallback)
    observer.observe(element, { attributes: true })
  }

  // 禁止 P2P
  Object.defineProperty(window, 'RTCPeerConnection', {
    get: () => undefined,
    set: () => { }
  })

  // 自动跳过广告
  findMatch(document.documentElement, 'iqpdiv.iqp-player-videolayer', (container) => {
    findMatch(container, 'video', (player) => {
      findMatch(container, 'div.bottom-public .skippable-after', (skipBtn) => {
        skipBtn.click()
        findMatch(container, '.cupid-public-time', (cpuidPublicTime) => {
          whenStyleChange(cpuidPublicTime, (target) => {
            if (target.style.display !== 'none') {
              skipBtn.click()
            }
          })
        }, true)
      }, true)
    }, true)
  })

  // 防止暂停后播放窗口变小，隐藏弹幕，不显示广告
  const style = document.createElement('style')
  style.innerText = ".flash-max{display: none!important}.iqp-player-videolayer{width:100%!important;height:100%!important}[data-player-hook=plgcontainer],.iqp-player-innerlayer{display:block!important}"
  document.head.appendChild(style)
})();