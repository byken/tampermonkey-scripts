// ==UserScript==
// @name         Bilibili Toolbox
// @namespace    http://tampermonkey.net/
// @version      0.4.2
// @description  Bilibili Toolbox.
// @author       byken
// @match        https://*.bilibili.com/*
// @icon         https://www.bilibili.com/favicon.ico
// @downloadURL  https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/bilibili-toolbox.user.js
// @updateURL    https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/bilibili-toolbox.user.js
// @run-at       document-start
// @grant        none
// ==/UserScript==

(function () {
  'use strict';

  const trackingKeyREList = [/^from_source/, /^vd_source/, /^spm_/, /^live_from/, /^search_source/, /broadcast_type/, /is_room_feed/]
  const mcdnHostRe = /[xy\d]+\.mcdn\.bilivideo\.cn:\d+/
  const isLiveHost = /live.bilibili.com\/\d+/.test(location.href)
  const playUrlFetchRe = /api.bilibili.com\/x\/player\/wbi\/playurl/

  /** @type {[(url: string, method: string) => void | false | Response]} */
  const fetchHookFns = []
  /** @type {[(url: string, data?: BodyInit | null) => void | false]} */
  const sendBeaconHookFns = []
  /** @type {[(xhr: XMLHttpRequest, url: string, method: string) => void | false | Event]} */
  const xhrHookFns = []

  // ======================== 工具方法 ========================
  const realFetch = fetch
  window.fetch = async function (...args) {
    const arg0 = args[0]
    const arg1 = args[1]
    let url = typeof arg0 === 'string'
      ? arg0
      : arg0 instanceof URL
        ? arg0.toString()
        : arg0 instanceof Request
          ? arg0.url
          : ''
    let method = arg0 instanceof Request
      ? arg0.method
      : arg1
        ? arg1.method
        : 'GET'
    for (let fn of fetchHookFns) {
      const result = fn(url, method)
      if (result !== undefined) {
        if (result === false) {
          return Promise.reject()
        } else {
          return result
        }
      }
    }
    return realFetch(...args)
  }

  const realSendBean = Navigator.prototype.sendBeacon
  Navigator.prototype.sendBeacon = function (...args) {
    for (let fn of sendBeaconHookFns) {
      if (fn(...args) === false) {
        return true
      }
    }
    return realSendBean.apply(this, args)
  }

  const realOpen = XMLHttpRequest.prototype.open
  const realSend = XMLHttpRequest.prototype.send
  XMLHttpRequest.prototype.open = function (method, url) {
    this._url = url
    this._method = method
    return realOpen.apply(this, arguments)
  }
  XMLHttpRequest.prototype.send = function (body) {
    for (let fn of xhrHookFns) {
      const result = fn(this, this._url, this._method)
      if (result === false) {
        return
      }
      if (result instanceof Event) {
        this.dispatchEvent(result)
        return
      }
    }
    return realSend.apply(this, arguments)
  }

  function copyFunctionsAndSetToNoop(source) {
    const noop = () => { }
    if (typeof source !== 'object') {
      return source
    }
    let target = {}
    const isFunction = (obj) => typeof obj === 'function'

    const copy = (source, target) => {
      // eslint-disable-next-line no-proto
      target.__proto__ = {}
      const descriptors = Object.getOwnPropertyDescriptors(source)
      Object.keys(descriptors).forEach((key) => {
        if (key === 'constructor' || !isFunction(descriptors[key].value)) {
          return
        }
        const types = [Object, String, Array, Function, Number, Boolean, Date, Error, RegExp, Map, Set, WeakMap, WeakSet, Symbol]
        for (let typ of types) {
          if (key in typ.prototype) {
            return
          }
        }
        target[key] = noop.bind(target)
      })
      // eslint-disable-next-line no-proto
      if (source.__proto__ === undefined || source.__proto__ === Object.prototype) {
        return
      }
      // eslint-disable-next-line no-proto
      target.__proto__ = target.__proto__ || {}
      // eslint-disable-next-line no-proto
      copy(source.__proto__, target.__proto__)
    }

    copy(source, target)
    return target
  }

  function addStyle(text) {
    const style = document.createElement('style')
    style.innerHTML = text.split('\n').map(it => it.trim()).join('')
    document.head.appendChild(style)
  }

  /**
   * @param mayBeUrl {string | URL}
   */
  function filterSpmEtc(mayBeUrl) {
    /**
     * @param search {string | URLSearchParams}
     * @returns
     */
    const filter = (search) => {
      const searchParams = new URLSearchParams(search)
      let keys = [...searchParams.keys()]
      for (let key of keys) {
        for (let reg of trackingKeyREList) {
          if (reg.test(key)) {
            searchParams.delete(key)
            break
          }
        }
      }
      if (typeof search === 'string') {
        return searchParams.toString()
      }
      return searchParams
    }
    if (typeof mayBeUrl === 'string') {
      const [originAndPath, other] = mayBeUrl.split('?', 2)
      let [search, hash] = other ? other.split('#', 2) : []
      if (search) {
        search = filter(search)
      }
      return `${originAndPath}${search ? `?${search}` : ''}${hash ? `#${hash}` : ''}`
    }
    mayBeUrl.searchParams = filter(mayBeUrl.searchParams)
    return mayBeUrl
  }

  const frozenProp = (val) => ({
    writable: false,
    configurable: false,
    enumerable: false,
    value: val,
  })

  const frozenEmptyObject = () => frozenProp(Object.freeze({}))

  const readonly = (val) => ({ get: () => val, set: () => { } })

  /**
   * @param {XMLHttpRequest} xhr
   * @param {string} content
   * @param {XMLHttpRequestResponseType} type
   */
  function xhrSet(xhr, content, type = 'text') {
    Object.defineProperties(xhr, {
      status: frozenProp(200),
      statusText: frozenProp('OK'),
      response: frozenProp(content),
      responseText: frozenProp(content),
      responseType: frozenProp(type),
    })
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @param {string} content
   */
  function xhrSetText(xhr, content) {
    xhrSet(xhr, content)
  }

  /**
   * @param {XMLHttpRequest} xhr
   * @param {string | Record<string, any>} content
   */
  function xhrSetJson(xhr, content) {
    xhrSet(xhr, typeof content === 'string' ? content : JSON.stringify(content), 'json')
  }

  function hasOwn(target, prop) {
    return Object.prototype.hasOwnProperty.call(target, prop)
  }
  // ======================== 工具方法结束 ========================

  // ======================== 去除打点日志 ========================
  let fakeReportObserver
  Object.defineProperty(window, 'reportObserver', {
    set: (value) => {
      fakeReportObserver = copyFunctionsAndSetToNoop(value)
    },
    get: () => {
      return fakeReportObserver
    }
  })
  sendBeaconHookFns.push((url) => {
    if (/\/log\/web/.test(url)) {
      return false
    }
  })
  xhrHookFns.push((xhr, url, method) => {
    if (/\/log\/web/.test(url)) {
      xhrSetText(xhr, 'ok')
    }
  })
  fetchHookFns.push((url, method) => {
    if (/\/log\/web/.test(url)) {
      return new Response('ok', { headers: { 'content-type': 'text/plain; charset=utf-8' } })
    }
  })
  // ======================== 去除打点日志结束 ========================

  // ======================== 去除跟踪参数 ========================
  const realCreateElement = Document.prototype.createElement
  Document.prototype.createElement = function fakeCreateElement(...args) {
    /**
     * @type {HTMLElement}
     */
    const element = realCreateElement.apply(this, args)
    if (args[0] === 'a') {
      element.addEventListener('click', (event) => {
        /**
         * @type {HTMLAnchorElement}
         */
        const ct = event.currentTarget
        if (/spm_id_from/.test(ct.href)) {
          ct.href = filterSpmEtc(ct.href)
        }
      }, true)
    }
    return element
  }
  var originalSet = Object.getOwnPropertyDescriptor(Element.prototype, 'innerHTML').set

  Object.defineProperty(Element.prototype, 'innerHTML', {
    set: function (value) {
      if (/spm_id_from/.test(value)) {
        try {
          value = value.replaceAll(/(<a[^>]+href=['""])([^'"]+)(['"][^>]+>)/g, (match, p1, p2, p3) => {
            return `${p1}${filterSpmEtc(p2)}${p3}`
          })
        } catch(ignored) {

        }
      }
      return originalSet.call(this, value)
    }
  })

  const realSetAttribute = HTMLAnchorElement.prototype.setAttribute
  HTMLAnchorElement.prototype.setAttribute = function fakeSetAttribute(...args) {
    if (args[0] === 'href' && /spm_id_from/.test(args[1])) {
      args[1] = filterSpmEtc(args[1])
    }
    return realSetAttribute.apply(this, args)
  }

  let realWindowOpen = window.open
  window.open = function (...args) {
    if (typeof args[0] === 'string' || args[0] instanceof URL) {
      args[0] = filterSpmEtc(args[0])
    }
    return realWindowOpen.apply(window, args)
  }

  // 禁止添加 vd_source
  const realReplaceState = History.prototype.replaceState
  History.prototype.replaceState = function fakeReplaceState(...args) {
    let hit = false
    if (args[2] instanceof URL) {
      hit = args[2].searchParams.has('vd_source')
    } else if (typeof args[2] === 'string') {
      hit = /vd_source/.test(args[2])
    }
    if (hit) {
      args[2] = filterSpmEtc(args[2])
    }
    return realReplaceState.apply(this, args)
  }
  // ======================== 去除跟踪参数结束 ========================


  // ======================== 视频资源请求 ========================
  if (hasOwn(window, 'RTCPeerConnection')) {
    Object.defineProperty(window, 'RTCPeerConnection', frozenProp(undefined))
  }
  if (hasOwn(window, 'mozRTCPeerConnection')) {
    Object.defineProperty(window, 'webkitRTCPeerConnection', frozenProp(undefined))
  }
  if (hasOwn(window, 'webkitRTCPeerConnection')) {
    Object.defineProperty(window, 'webkitRTCPeerConnection', frozenProp(undefined))
  }

  fetchHookFns.push((url) => {
    if (mcdnHostRe.test(url)) {
      return false
    }
  })

  const filterVideoSources = (res) => {
    const doFilter = (obj, key) => {
      if (!obj) {
        return
      }
      const items = [...obj[key]]
      if (!items || items.length === 0) {
        return
      }
      const newItems = []
      for (let item of items) {
        const baseUrl = item.baseUrl || item.base_url
        const newItem = Object.assign({}, item)
        /** @type {string[]} */
        const backupUrls = (item.backupUrl || item.backup_url || []).filter(item => !mcdnHostRe.test(item))
        if (mcdnHostRe.test(baseUrl)) {
          if (backupUrls.length === 0) {
            continue
          }
          newItem.baseUrl = backupUrls[0]
          newItem.base_url = backupUrls[0]
        }
        newItem.backupUrl = backupUrls
        newItem.backup_url = backupUrls
        newItems.push(newItem)
      }
      if (newItems.length > 0) {
        obj[key] = newItems
      }
    }
    doFilter(res?.data?.dash, 'video')
    doFilter(res?.data?.dash, 'audio')
    return res
  }
  let playInfo
  Object.defineProperty(window, '__playinfo__', {
    get: () => playInfo,
    set: (value) => {
      playInfo = filterVideoSources(value)
    }
  })
  xhrHookFns.push((xhr, url, method) => {
    if (playUrlFetchRe.test(url)) {
      const onload = () => {
        xhrSetJson(xhr, filterVideoSources(JSON.parse(xhr.responseText)))
        xhr.removeEventListener('load', onload)
      }
      xhr.addEventListener('load', onload, true)
    }
  })
  // ======================== 视频资源请求 ========================

  // ======================== 播放器体验优化 ========================
  if (isLiveHost) {
    /** @type {HTMLElement | null} */
    document.addEventListener('keydown', (event) => {
      if (['TEXTAREA', 'INPUT'].includes(document.activeElement?.tagName)) {
        // ignore normal edit event
        return
      }
      if (event.ctrlKey || event.metaKey || event.altKey || event.shiftKey) {
        // 忽略组合键
        return
      }
      /** @type {HTMLVideoElement | null} */
      const video = document.querySelector('#live-player')
      if (!video) {
        return
      }
      if (event.code === 'KeyF') {
        if (document.fullscreenElement) {
          document.exitFullscreen()
        } else {
          video.requestFullscreen()
        }
      } else if (event.code === 'KeyM') {
        const realVideo = video.querySelector('video')
        if (realVideo) {
          realVideo.muted = !realVideo.muted
        }
      }
    })
  }
  // ======================== 播放器体验优化结束 ========================

  // ======================== 其他（去除广告拦截提示等） ========================
  const realBoundStorageGetItem = localStorage.getItem.bind(localStorage)
  let localStorageProto
  if (/firefox/i.test(navigator.userAgent)) {
    // eslint-disable-next-line no-proto
    localStorageProto = localStorage.__proto__
  } else {
    localStorageProto = localStorage
  }
  localStorageProto.getItem = (key) => {
    if (key === 'IS_SHOW_TIP_TD') {
      return JSON.stringify(`${new Date().getFullYear()}${new Date().getMonth() + 1}${new Date().getDate()}_1`)
    } else if (key === 'IS_SHOW_TIP_TD-timeout') {
      return Date.now() + 1e6
    }
    return realBoundStorageGetItem(key)
  }

  Object.defineProperty(window, 'abtest', frozenEmptyObject())
  Object.defineProperty(window, 'onerror', readonly(frozenEmptyObject()))
  const fakeBiliCm = new Proxy({}, { get: () => { }, set: () => true })
  Object.defineProperty(window, 'BiliCm', frozenProp(fakeBiliCm))
  Object.defineProperty(window, 'bilicm', frozenProp(fakeBiliCm))
  Object.defineProperty(window, 'ad_rp', readonly(frozenEmptyObject()))
  if (isLiveHost) {
    addStyle(`
      #full-screen-interactive-wrap
      {
        display: none !important
      }
      .fullscreen-danmaku{
        bottom: 0 !important
      }
      .floor-single-card{
        margin-top: 0 !important
      }
      `
    )
  } else {
    addStyle(`
      .feed-card:has(a[href*=".bilibili.com"][data-target-url]:not([data-target-url*=".bili"])),
      .feed-card:has([class*=extension-tips]),
      .feed-card:not(:has(img)),
      .feed-card:has(.bili-video-card__info--ad),
      .bili-video-card:has([class*=extension-tips]),
      .bili-video-card:has(.bili-video-card__info--ad),
      .bili-video-card:not(:has(img)),
      .vui_carousel__slide:has(a[href*="cm.bilibili.com"][data-target-url]:not([data-target-url*=".bilibili.com/"])),
      .vui_carousel__slide:has(> [class*="carousel-item"] > img),
      .vui_carousel__slide:has(img[src*="long/images/eva.png"]),
      .floor-single-card .layer
      {
        display: none !important
      }
      .feed-card,
      .bili-video-card,
      .floor-single-card,
      .load-more-anchor
      {
        margin-top: 0 !important
      }
      .recommended-swipe-shim .bili-video-card {
        display: block !important
      }
      .bili-live-card {
        margin-top: 0 !important
      }
      `
    )
  }
  // ======================== 其他（去除广告拦截提示等）结束 ========================
})()