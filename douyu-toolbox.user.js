// ==UserScript==
// @name         Douyu Toolbox
// @namespace    http://tampermonkey.net/
// @version      0.1.5
// @description  Douyu Toolbox.
// @author       byken
// @match        https://*.douyu.com/*
// @icon         https://www.douyu.com/favicon.ico
// @downloadURL  https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/douyu-toolbox.user.js
// @updateURL    https://gitlab.com/byken/tampermonkey-scripts/-/raw/master/douyu-toolbox.user.js
// @run-at       document-start
// @grant        none
// ==/UserScript==

(function () {
  'use strict';
  const href = window.location.href
  const isRoomPage = /^https?:\/\/www.douyu.com\/\d+/.test(href)
  const isTopicPage = /^https?:\/\/www.douyu.com\/topic/.test(href)
  if (isRoomPage) {
    const style = document.createElement('style')
    style.innerText = `
      body {
        min-width: unset;
      }
      [id*="activity"],
      .layout-Player-announce,
      .layout-Player-rank,
      .layout-Player-rankAll,
      .PlayerToolbar,
      .layout-Aside,
      .layout-Player-title,
      .layout-Bottom {
        display: none !important;
      }
      .layout-Player-barrage {
        top: 0 !important;
      }
      .layout-Aside,
      .layout-Player-toolbar {
        height: 0 !important;
      }
      .layout-Main {
        padding-left: 0 !important;
        padding-right: 0 !important;
        margin-left: 0 ! important;
      }
      .layout-Container {
        box-sizing: border-box !important;
        height: 100vh !important;
      }
      .layout-Main,
      .layout-Player,
      .layout-Player-main {
        box-sizing: border-box !important;
        box-sizing: border-box !important;
        height: 100% !important;
      }
      .layout-Player-video {
        overflow: initial !important;
        position: initial !important;
        height: initial !important;
        padding-top: initial !important;
        clear: initial !important;
      }
      body.is-fullScreenPage .layout-Player-aside {
        display: none !important;
      }
      body.is-fullScreenPage .layout-Player-main {
        width: 100vw;
      }
      .Header-menu-link:has(.public-DropMenu.Game) {
        display: none;
      }
    `.split('\n').map(it => it.trim()).join('')
    document.head.appendChild(style)
  }
  if (isRoomPage || isTopicPage) {
    document.addEventListener('keydown', (event) => {
      if (['TEXTAREA', 'INPUT'].includes(document.activeElement?.tagName)) {
        // ignore normal edit event
        return
      }
      if (event.ctrlKey || event.metaKey || event.altKey || event.shiftKey) {
        // 忽略组合键
        return
      }
      if (event.code === 'KeyF') {
        if (document.fullscreenElement) {
          document.exitFullscreen()
        } else {
          document.querySelector('[title="全屏"]')?.click?.()
        }
      } else if (event.code === 'KeyM') {
        document.querySelector('[title$="静音"]')?.click?.()
      } else if (event.code === 'KeyH') {
        // 清晰度
        document.querySelector('[title="清晰度"] ul li')?.click?.()
      } else if (event.code === 'KeyN') {
        const noMoreCheckbox = document.querySelector('.GuessGameMiniPanelB-noMore:not(.is-active)')
        if (noMoreCheckbox) {
          noMoreCheckbox.click()
        }
        const closeButton = document.querySelector('.GuessGameMiniPanelB-closeIcon')
        if (closeButton) {
          closeButton.click()
        }
      }
    })
  }
  ;['RTCPeerConnection', 'webkitRTCPeerConnection', 'mozRTCPeerConnection'].forEach(item => {
    Object.defineProperty(window, item, {
      get: () => undefined,
      set: () => { }
    })
  })
  const realAddEventListener = EventTarget.prototype.addEventListener
  EventTarget.prototype.addEventListener = function(...args) {
    if ((this === window || this === document) && (args[0] === 'blur' || args[0] === 'focus')) {
      return
    }
    return realAddEventListener.apply(this, args)
  }
})();